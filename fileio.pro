CDPVERSION = 4.0.0
DEPS += \

HEADERS += \
    fileio.h \
    fileioBuilder.h \
    fileioserver.h \
    gpio.h

SOURCES += \
    fileioBuilder.cpp \
    fileioserver.cpp \
    gpio.cpp

DISTFILES += \
    Models/* \
    Models/FileIO.GPIO.xml \
    Models/FileIOServer.xml

include($$(CDPBASE)/qmakefeatures/cdplibrary.pri)