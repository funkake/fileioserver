/**
fileio header file. Include this file in the project to use the library.
*/
#ifndef FILEIO_H
#define FILEIO_H

#include "fileioBuilder.h"

/** Instantiate the fileiobuilder for this object */
FileIOServerLibBuilder gFileIOServerLibBuilder("fileio", __TIMESTAMP__);

#endif // FILEIOSERVERLIB_H

