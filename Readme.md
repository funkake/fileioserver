FileIOServer
====
The FileIOServer implements file based IOServer for CDP Studio to be used on embedded linux targets to access GPIO and other peripherals to support Raspberry Pi and others

Direct dependencies
----
This projet is intended to be built in CDP Studio IDE

Platforms
----
Linux

Usage
----
It is intended to be used by CDP Studio to create embedded robotix projects

Maintainers
----
Jüri Toomessoo

